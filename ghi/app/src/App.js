import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './new-conference';
import AttendConferenceForm from './AttendConferenceForm';
import React from "react";
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage/>}/>
          <Route path="presentations/new" element={<PresentationForm />}/>
          <Route path="attendees/new" element={<AttendConferenceForm />}/>
          <Route path="conferences/new" element={<ConferenceForm />}/>
          <Route path="locations/new" element={<LocationForm />}/>
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
