import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  // show dropdown of all possible locations:
  const [locations, setLocations] = useState([]);

  // form states:
  const [location, setLocation] = useState('');
  const [name, setName] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [description, setDescription] = useState('');
  const [maxPresentations, setMaxPresentations] = useState('');
  const [maxAttendees, setMaxAttendees] = useState('');

  const handleLocationChange = async (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const handleNameChange = async (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleStartDateChange = async (event) => {
    const value = event.target.value;
    setStartDate(value);
  }

  const handleEndDateChange = async (event) => {
    const value = event.target.value;
    setEndDate(value);
  }

  const handleDescriptionChange = async (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const handleMaxPresentationsChange = async (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  }

  const handleMaxAttendeesChange = async (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    // create an empty JSON object (data) which is going to be
    // our request body
    const data = {};

    data.location = location;
    data.name = name;
    data.description = description;
    data.max_presentations = maxPresentations;
    data.max_attendees = maxAttendees;
    data.starts = startDate;
    data.ends = endDate;

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      setName('');
      setDescription('');
      setEndDate('');
      setStartDate('');
      setMaxAttendees('');
      setMaxPresentations('');
      setLocation('');
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';


    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartDateChange} required type="date" name="starts" id="starts" className="form-control" value={startDate}/>
                <label htmlFor="starts">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndDateChange} required type="date" name="ends" id="ends" className="form-control" value={endDate}/>
                <label htmlFor="ends">End Date</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={handleDescriptionChange} id="description" required name="description" className="form-control" value={description}></textarea>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationsChange} required type="number" name="max_presentations" id="max_presentations" className="form-control" value={maxPresentations}/>
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange} required type="number" name="max_attendees" id="max_attendees" className="form-control" value={maxAttendees}/>
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required name="location" id="location" className="form-select" value={location}>
                  <option>Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  );
}

export default ConferenceForm;
