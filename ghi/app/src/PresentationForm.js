import React, { useEffect, useState } from 'react'

function PresentationForm() {
  // show dropdown of all possible conferences:
  const [conferences, setConferences] = useState([]);

  // selecting a conference from the drop down and saving it's
  // state in the conference value it's on line 52
  const [conference, setConference] = useState([]);

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  }

  // set the useState hook to store components' state,
  // with a default initial value of an empty string
  const [presenterName, setPresenterName] = useState('');
  const [presenterEmail, setPresenterEmail] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');


  const handlePresenterNameChange = (event) => {
    const value = event.target.value;
    setPresenterName(value);
  }

  const handlePresenterEmailChange = (event) => {
    const value = event.target.value;
    setPresenterEmail(value);
  }

  const handleCompanyNameChange = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  }

  const handleTitleChange = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  }

  const handleSynopsisChange = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  }

  async function handleSubmit(event) {
    event.PreventDefault();

    const data = {};

    data.title = title;
    data.presenter_name = presenterName;
    data.company_name = companyName;
    data.presenter_email = presenterEmail;
    data.synopsis = synopsis;

    const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      setTitle('');
      setPresenterName('');
      setCompanyName('');
      setPresenterEmail('');
      setSynopsis('');
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div class="row">
        <div class="offset-3 col-6">
          <div class="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div class="form-floating mb-3">
                <input onChange={handlePresenterNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" class="form-control"/>
                <label for="presenter_name">Presenter name</label>
              </div>
              <div class="form-floating mb-3">
                <input onChange={handlePresenterEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" class="form-control"/>
                <label for="presenter_email">Presenter email</label>
              </div>
              <div class="form-floating mb-3">
                <input onChange={handleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" class="form-control"/>
                <label for="company_name">Company name</label>
              </div>
              <div class="form-floating mb-3">
                <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" class="form-control"/>
                <label for="title">Title</label>
              </div>
              <div class="mb-3">
                <label for="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} class="form-control" id="synopsis" rows="3" name="synopsis" class="form-control"></textarea>
              </div>
              <div class="mb-3">
                <select onChange={handleConferenceChange} required name="conference" id="conference" class="form-select">
                  <option selected value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                      <option key={conference.id} value={conference.id}>
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button class="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  );
}

export default PresentationForm;
