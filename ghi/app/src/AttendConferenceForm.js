import React, { useEffect, useState } from 'react';

function AttendConferenceForm() {
  // show dropdown of all possible conferences:
  const [conferences, setConferences] = useState([]);

  // form states:
  const[conference, setConference] = useState('');
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');

  const handleEmailChange = async (event) => {
    const value = event.target.value;
    setEmail(value);
  }

  const handleNameChange = async (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleConferenceChange = async (event) => {
    const value = event.target.value;
    setConference(value);
  }
  // line 27 is written a different way but is the exact same as line 22
  async function handleSubmit(event) {
    event.preventDefault();

    const data = {};

    data.email = email;
    data.name = name;

    const attendeeUrl = `http://localhost:8001/api/conferences/${conference}/attendees/`;
    console.log(data)
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
      const newAttendee = await response.json();
      setName('');
      setEmail('');
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (conferences.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  return (
    <div className="my-5 container">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className={spinnerClasses} id="loading-conference-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleConferenceChange} name="conference" id="conference" className={dropdownClasses} required>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                      return (
                        <option key={conference.id} value={conference.id}>
                          {conference.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
  )
}

export default AttendConferenceForm;
